/*jshint node:true*/

(function() {
	//var jsdom   = require('jsdom');
	var cheerio = require('cheerio'); // temp - for working on windows

	var request = require('request');

	var express = require('express');


	// http request defaults
	var r = request.defaults({
		//proxy: 'http://localhost:4242',
		headers: {
			'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Language': 'en-US,en;q=0.5',
			'Connection': 'keep-alive'
		},
		followRedirect: true
	});


	// http server

	var app = express();

	app.get('/', function(req, res){
		var toBeSent = '';
		r.get('http://langtons.com.au/Auction/BrowseDetail.aspx?Auction_Id=2148&Lot_Num=1', function(error, response, body) {
			if (!error) {
				$ = cheerio.load(body);

				toBeSent += '<h1>Current Auctions</h1>';
				toBeSent += $('#ctl00_cphMain_LotList1_tblResults').html();
			}
		});

		r.get('https://langtons.com.au/Home/login.aspx',
			function(error, response, body) {
				$ = cheerio.load(body);

				r.post(
					{
						url: 'https://langtons.com.au/Home/login.aspx',
						form: {
							__EVENTTARGET: 'ctl00$cphMain$LoginStandard1$ibnLogin',
							__EVENTARGUMENT: '',
							__VIEWSTATE: $('#__VIEWSTATE').attr('value'),
							__SCROLLPOSITIONX: 0,
							__SCROLLPOSITIONY: 0,
							__EVENTVALIDATION: $('#__EVENTVALIDATION').attr('value'),
							'ctl00$cphMain$LoginStandard1$txtUserName': 'testaccount001',
							'ctl00$cphMain$LoginStandard1$txtPassword': 'qwerty'
						}
					},
					function(error, response, body) {
						if (error) {
							console.log(error);
						} else {
							r.get('https://langtons.com.au/Accounts/Intro.aspx', function(error, response, body) {
								$ = cheerio.load(body);

								toBeSent += '<h1>My Account Details</h1>';
								toBeSent += '<table>' + $('.tableWhiteBox.summaryFirstColumn').html() + '</table>';

								r.get('https://langtons.com.au/Accounts/History.aspx?Level=Buying', function(error, response, body) {
									$ = cheerio.load(body);

									toBeSent += '<h1>My Buying History</h1>';
									toBeSent += '<table>' + $('.tableWhiteBox.tableWhiteBoxBorder.historySecondColumn') + '</table>';

									res.send(toBeSent);
								});
							});
						}
					}
				);
			}
		);
	});

	app.listen(12345);
}());
